package org.nmenkovic.microservices.restcontrollers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.nmenkovic.microservices.common.Message;
import org.nmenkovic.microservices.common.dto.PersonDTO;
import org.nmenkovic.microservices.kafka.KafkaProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
public class PersonDTOController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonDTOController.class);

    @Autowired
    private KafkaProducer kafkaProducer;

    @RequestMapping(value = "/persons", method = RequestMethod.GET)
    public List<PersonDTO> getPersons() throws JsonProcessingException {
        LOGGER.info("PersonDTOController.getPersons");

        Message message = new Message();
        message.setOperation("getAll");

        ObjectMapper objectMapper = new ObjectMapper();
        String messageString = objectMapper.writeValueAsString(message);

        // TODO: remove hardcoded topic
        // TODO: wait for response from the other service
        kafkaProducer.send("rest-db", messageString);

        return Collections.emptyList();
    }

}